package com.mastertech.pagamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PagamentoMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagamentoMicroserviceApplication.class, args);
	}

}
