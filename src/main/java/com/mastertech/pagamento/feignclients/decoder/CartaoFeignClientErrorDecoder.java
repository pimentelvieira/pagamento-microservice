package com.mastertech.pagamento.feignclients.decoder;

import com.mastertech.pagamento.exceptions.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.http.HttpStatus;

public class CartaoFeignClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == HttpStatus.BAD_REQUEST.value()) {
            return new CartaoNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}

