package com.mastertech.pagamento.controllers;

import com.mastertech.pagamento.models.Pagamento;
import com.mastertech.pagamento.models.dtos.CreatePagamentoRequest;
import com.mastertech.pagamento.models.dtos.PagamentoResponse;
import com.mastertech.pagamento.models.mappers.PagamentoMapper;
import com.mastertech.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponse create(@RequestBody CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = PagamentoMapper.toPagamento(createPagamentoRequest);

        pagamento = pagamentoService.create(pagamento);

        return PagamentoMapper.toPagamentoResponse(pagamento);
    }

    @GetMapping("/pagamentos/{cartaoId}")
    public List<PagamentoResponse> listByCartao(@PathVariable Integer cartaoId) {
        List<Pagamento> pagamentos = pagamentoService.listByCartao(cartaoId);
        return PagamentoMapper.toPagamentoResponse(pagamentos);
    }

}

