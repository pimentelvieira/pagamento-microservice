package com.mastertech.pagamento.services;

import com.mastertech.pagamento.feignclients.CartaoFeignClient;
import com.mastertech.pagamento.models.Pagamento;
import com.mastertech.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoFeignClient cartaoFeignClient;

    public Pagamento create(Pagamento pagamento) {
        this.cartaoFeignClient.getCartaoById(pagamento.getCartaoId());
        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> listByCartao(Integer cartaoId) {
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }
}

