package com.mastertech.pagamento.feignclients.configuration;

import com.mastertech.pagamento.feignclients.decoder.CartaoFeignClientErrorDecoder;
import com.mastertech.pagamento.feignclients.fallback.CartaoFeignClientFallback;
import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CartaoFeignClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new CartaoFeignClientErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(CartaoFeignClientFallback::new)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
