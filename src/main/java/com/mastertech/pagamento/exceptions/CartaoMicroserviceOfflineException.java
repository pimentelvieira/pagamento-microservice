package com.mastertech.pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Microserviço de cartões offline")
public class CartaoMicroserviceOfflineException extends RuntimeException {
}

