package com.mastertech.pagamento.feignclients.fallback;

import com.mastertech.pagamento.exceptions.CartaoMicroserviceOfflineException;
import com.mastertech.pagamento.feignclients.CartaoFeignClient;
import com.netflix.client.ClientException;

public class CartaoFeignClientFallback implements CartaoFeignClient {

    private Exception cause;

    public CartaoFeignClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public void getCartaoById(Integer id) {
        if(cause.getCause() instanceof ClientException) {
            throw new CartaoMicroserviceOfflineException();
        } else {
            throw (RuntimeException) cause;
        }
    }
}

