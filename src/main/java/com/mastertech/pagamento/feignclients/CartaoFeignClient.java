package com.mastertech.pagamento.feignclients;

import com.mastertech.pagamento.feignclients.configuration.CartaoFeignClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "cartao-microservice", configuration = CartaoFeignClientConfiguration.class)
@RequestMapping("/cartao")
public interface CartaoFeignClient {

    @GetMapping("/id/{id}")
    void getCartaoById(@PathVariable Integer id);
}
