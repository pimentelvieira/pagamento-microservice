package com.mastertech.pagamento.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PagamentoResponse {

    private Integer id;

    private String descricao;

    private Double valor;

    @JsonProperty("cartao_id")
    private Integer cartaoId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Integer cartaoId) {
        this.cartaoId = cartaoId;
    }
}

